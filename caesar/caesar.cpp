#include<iostream>
#include<string>
#include<vector>
#include<math.h>
using namespace std;
string arr = "abcdefghijklmnopqrstuvwxyz";
void encrypt(string p, int k);
void decrypt(string c, int k);

int main()
{
	string plainText = "";
	int key, i=0,choice;
	cout << "Enter plain text: ";
	getline(cin, plainText);
	cout << endl;
	cout << "Enter key: ";
	cin>>key;
	cout << "1. Encrypt\n" << "2. Decrypt\n";
	cin >> choice;
	switch (choice)
	{
	case 1: encrypt(plainText,key);
		cout << endl;
		break;
	case 2: decrypt(plainText, key);
		cout << endl;
		break;
	default: cout << "Error!" << endl;
		break;
	}
	cout << endl;
	system("pause");
	return 0;
}
void encrypt(string p, int k)
{
	int i = 0;
	vector<char> cipherText;
	while (i <= p.length())
	{
		for (int j = 0; j < 26; j++)
		{
			if (p[i] == arr[j])
			{
				cipherText.push_back(arr[(j + k) % 26]);
			}
		}
		i++;
	}
	for (int i = 0; i < cipherText.size(); i++)
	{
		cout << cipherText[i];
	}
}

void decrypt(string c, int k)
{
	int i = 0;
	vector<char> cipherText;
	while (i <= c.length())
	{
		for (int j = 0; j < 26; j++)
		{
			if (c[i] == arr[j])
			{
				cipherText.push_back(arr[((j%26)+26-(k%26))%26]);
			}
		}
		i++;
	}
	for (int i = 0; i < cipherText.size(); i++)
	{
		cout << cipherText[i];
	}
}
